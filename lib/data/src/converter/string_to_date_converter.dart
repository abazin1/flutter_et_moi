import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

class StringToDate extends JsonConverter<DateTime, String> {
  const StringToDate();

  @override
  DateTime fromJson(String json) {
    try {
      final day = int.tryParse(json.substring(0, 1)) ?? 1;
      final month = int.tryParse(json.substring(2, 3)) ?? 1;
      final year = int.tryParse(json.substring(4, 8)) ?? 2023;
      return DateTime(year, month, day);
    } on FormatException catch (e) {
      debugPrint(e.toString());
      return DateTime.now();
    }
  }

  @override
  String toJson(DateTime object) {
    return '${object.day}${object.month}${object.year}';
  }
}
