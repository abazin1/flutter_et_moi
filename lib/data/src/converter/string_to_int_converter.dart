import 'package:freezed_annotation/freezed_annotation.dart';

class StringToInt extends JsonConverter<int, String> {
  const StringToInt();

  @override
  int fromJson(String json) {
    return json.isEmpty ? 0 : int.tryParse(json) ?? -1;
  }

  @override
  String toJson(int object) {
    return object.toString();
  }
}
