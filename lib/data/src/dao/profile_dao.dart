import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:name_of_project/data/src/entity/profile/profile_entity.dart';
import 'package:name_of_project/data/src/http/http.dart';

final profileDaoHttpProvider = Provider((ref) => _ProfileDao(ref));

class _ProfileDao {
  _ProfileDao(this.ref);

  final Ref ref;
  final Dio dio = Http.instance.dio;

  Future<ProfileDto> get() async {
    const url = 'user';

    try {
      final resp = await dio.get<Map<String, dynamic>>(url);
      return ProfileDto.fromJson(resp.data ?? {});
    } on Exception catch (e) {
      debugPrint(e.toString());
      return ProfileDto();
    }
  }
}
