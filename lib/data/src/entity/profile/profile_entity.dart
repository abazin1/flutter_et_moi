import 'package:freezed_annotation/freezed_annotation.dart';

part 'profile_entity.freezed.dart';
part 'profile_entity.g.dart';

@freezed
abstract class ProfileEntity with _$ProfileEntity {
  ProfileEntity._();

  factory ProfileEntity.dto({
    @Default('') String user,
    @Default('') String name,
    @Default('') String email,
    @Default('') String language,
  }) = ProfileDto;

  factory ProfileEntity.record({
    @Default('') String user,
    @Default('') String name,
    @Default('') String email,
    @Default('') String language,
  }) = ProfileRecord;

  factory ProfileEntity.fromJson(Map<String, Object?> json) => _$ProfileEntityFromJson(json);
}
