import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class Http {
  Http._();
  static final Http _singleton = Http._();

  static Http get instance => _singleton;

  Dio? _dio;

  Dio get dio {
    if (_dio == null) {
      throw Exception('Call init before use Http.instance.dio');
    }
    return _dio!;
  }

  static void init() {
    final dio = Dio(
      BaseOptions(
        baseUrl: dotenv.get('HTTP_BASE_URL'),
        maxRedirects: int.tryParse(dotenv.get('HTTP_MAX_REDIRECTS')),
        connectTimeout: Duration(milliseconds: int.tryParse(dotenv.get('HTTP_TIMEOUT'), radix: 10) ?? 60000),
        receiveTimeout: Duration(milliseconds: int.tryParse(dotenv.get('HTTP_TIMEOUT'), radix: 10) ?? 60000),
        sendTimeout: Duration(milliseconds: int.tryParse(dotenv.get('HTTP_TIMEOUT'), radix: 10) ?? 60000),
        headers: <String, dynamic>{
          'content-type': Headers.jsonContentType,
        },
      ),
    );

    // if (kDebugMode)
    dio.interceptors.add(PrettyDioLogger());
    Http.instance._dio = dio;
  }
}
