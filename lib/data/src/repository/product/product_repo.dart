import 'package:hive_flutter/hive_flutter.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:name_of_project/data/src/entity/product/product_entity.dart';

const productsBoxName = 'products';
final Box<ProductRecord> _products = Hive.box<ProductRecord>(productsBoxName);

final productRepoProvider = Provider(_ProductRepository.new);

class _ProductRepository {
  _ProductRepository(this.ref);

  final Ref ref;

  /// Add Product to Database
  Future<List<ProductRecord>> add(ProductRecord entity) async {
    await _products.add(entity);
    return _products.values.toList();
  }

  /// Add Product to Database
  Future<List<ProductRecord>> addAll(List<ProductRecord> entities) async {
    await _products.addAll(entities);
    return _products.values.toList();
  }

  /// Fetch the Product from the "products" database
  List<ProductRecord> getAll() {
    return _products.values.toList();
  }

  ProductRecord get({required String sku}) {
    return _products.values.firstWhere((element) => element.sku == sku, orElse: () => ProductRecord(id: '0'));
  }

  /// Update Product
  Future<List<ProductRecord>> update(int index, ProductRecord entity) async {
    await _products.putAt(index, entity);
    return _products.values.toList();
  }

  /// Remove Particular Product by id
  Future<List<ProductRecord>> remove(String id) async {
    await _products.deleteAt(_products.values.toList().indexWhere((element) => element.id == id));
    return _products.values.toList();
  }

  /// Remove Particular Product
  Future<void> clear() async {
    await _products.clear();
  }
}
