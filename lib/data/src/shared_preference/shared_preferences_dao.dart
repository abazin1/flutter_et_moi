import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'shared_preferences_dao.g.dart';

@riverpod
SharedPreferencesDao sharedPreferencesDao(SharedPreferencesDaoRef ref) => throw UnimplementedError();

class SharedPreferencesDao {
  SharedPreferencesDao({
    required this.sharedPreferences,
  });

  final SharedPreferences sharedPreferences;

  final _languageCode = 'languageCode';

  Locale get locale {
    final languageCode = sharedPreferences.getString(_languageCode) ?? dotenv.get('APP_DEFAULT_LANG');
    return Locale(languageCode);
  }

  Future<void> setLocale(Locale locale) async => sharedPreferences.setString(_languageCode, locale.languageCode);

  Future<bool> clearAll() => sharedPreferences.clear();
}
