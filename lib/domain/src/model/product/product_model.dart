import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:name_of_project/data/src/entity/product/product_entity.dart';

part 'product_model.freezed.dart';

@freezed
abstract class ProductModel with _$ProductModel {
  ProductModel._();

  factory ProductModel({
    @Default('') String sku,
    @Default(-1) double price,
    @Default(-1) double discountPrice,
    @Default(1) double discountRate,
  }) = _Data;

  static ProductModel fromEntity({
    required ProductRecord entity,
  }) =>
      ProductModel(
        sku: entity.sku,
        price: entity.price,
        discountPrice: entity.discountPrice,
        discountRate: entity.discountRate,
      );

  static fromRecord({
    required ProductRecord record,
  }) {
    return ProductModel(
      sku: record.sku,
      price: record.price,
      discountPrice: record.discountPrice,
      discountRate: record.discountRate,
    );
  }
}
