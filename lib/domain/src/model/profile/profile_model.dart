import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:name_of_project/data/src/entity/profile/profile_entity.dart';

part 'profile_model.freezed.dart';

@freezed
class ProfileModel with _$ProfileModel {
  ProfileModel._();

  factory ProfileModel({
    @Default('') String user,
    @Default('') String name,
    @Default('') String email,
    @Default('') String language,
  }) = _Data;

  static ProfileModel fromEntity({
    required ProfileDto entity,
  }) =>
      ProfileModel(
        name: entity.name,
        user: entity.user,
        email: entity.email,
        language: entity.language,
      );
}
