import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:name_of_project/data/src/shared_preference/shared_preferences_dao.dart';
import 'package:name_of_project/domain/src/notifier/profile/profile_notifier.dart';
import 'package:name_of_project/l10n/generated/app_localizations.dart';

final localeProvider = NotifierProvider.autoDispose<_LocaleNotifier, Locale>(_LocaleNotifier.new);

class _LocaleNotifier extends AutoDisposeNotifier<Locale> {
  @override
  Locale build() {
    final profile = ref.watch(profileNotifierProvider);

    if (profile.language.isNotEmpty && AppLocalizations.supportedLocales.contains(Locale(profile.language))) {
      return Locale(profile.language);
    }
    return ref.watch(sharedPreferencesDaoProvider).locale;
  }

  Locale get locale => state;

  set locale(Locale locale) {
    ref.watch(sharedPreferencesDaoProvider).setLocale(locale);
    state = locale;
  }
}
