import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:name_of_project/data/src/dao/product_dao.dart';
import 'package:name_of_project/data/src/entity/product/product_entity.dart';
import 'package:name_of_project/data/src/repository/product/product_repo.dart';
import 'package:name_of_project/domain/src/model/product/product_model.dart';

final productServiceProvider = Provider((ref) => _ProductService(ref));

class _ProductService {
  _ProductService(this.ref);

  final Ref ref;

  List<ProductModel> getAll() {
    final records = ref.read(productRepoProvider).getAll();
    return records.map<ProductModel>((record) => ProductModel.fromRecord(record: record)).toList(growable: false);
  }

  Future<List<ProductModel>> fetchAll() async {
    try {
      final products = await ref.read(productDaoHttpProvider).fetchAll();

      await ref.read(productRepoProvider).clear();
      final records = products.map((product) => ProductEntity.toRecord(dto: product)).toList(growable: false);
      await ref.read(productRepoProvider).addAll(records);

      return records.map<ProductModel>((record) => ProductModel.fromRecord(record: record)).toList(growable: false);
    } catch (e) {
      debugPrint(e.toString());
      return <ProductModel>[];
    }
  }
}
