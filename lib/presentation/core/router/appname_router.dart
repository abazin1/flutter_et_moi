import 'package:go_router/go_router.dart';
import 'package:name_of_project/presentation/core/router/appname_route.dart';
import 'package:name_of_project/presentation/feature/ArticleSheet/article_sheet_page.dart';
import 'package:name_of_project/presentation/feature/PriceTracking/price_tracking_page.dart';
import 'package:name_of_project/presentation/feature/TransferRequest/transfer_request_page.dart';
import 'package:name_of_project/presentation/feature/login/login_page.dart';
import 'package:name_of_project/presentation/feature/welcome/welcome_page.dart';
import 'package:name_of_project/presentation/feature/reception/reception_page.dart';
import 'package:name_of_project/presentation/feature/Dispatch/dispatch_page.dart';

final smaRouter = GoRouter(
  routes: <RouteBase>[
    GoRoute(
      path: '/',
      name: kLoginPage,
      builder: (context, state) {
        return LoginPage();
      },
    ),
    GoRoute(
      path: '/welcome',
      name: kWelcomePage,
      builder: (context, state) => const WelcomePage(),
    ),
    GoRoute(
      path: '/reception',
      name: kReception,
      builder: (context, state) => const ReceptionPage(),
    ),
    GoRoute(
      path: '/expedition',
      name: kDispatch,
      builder: (context, state) => const DispatchPage(),
    ),
    GoRoute(
      path: '/Transfer',
      name: kTransfer,
      builder: (context, state) => const TransferPage(),
    ),
    GoRoute(
      path: '/PriceTracking',
      name: kPriceTracking,
      builder: (context, state) => const PriceTrackingPage(),
    ),
    GoRoute(
      path: '/ArticleSheet',
      name: kArticleSheet,
      builder: (context, state) => const ArticleSheetPage(),
    ),
  ],
);
