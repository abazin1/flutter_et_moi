import 'package:flutter/material.dart';
import 'package:name_of_project/presentation/util/extension/context_extension.dart';
import 'package:name_of_project/presentation/widget/functional/scaffold/appname_scaffold.dart';

class ArticleSheetPage extends StatelessWidget {
  const ArticleSheetPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppNameScaffold(
      title: context.i18n.menuArticleSheet,
      child: const Center(
        child: Text(
          'Fiche Article',
          style: TextStyle(fontSize: 30),
        ),
      ),
    );
  }
}
