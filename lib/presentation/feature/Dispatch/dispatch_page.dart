import 'package:flutter/material.dart';
import 'package:name_of_project/presentation/util/extension/context_extension.dart';
import 'package:name_of_project/presentation/widget/functional/scaffold/appname_scaffold.dart';

class DispatchPage extends StatelessWidget {
  const DispatchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppNameScaffold(
      title: context.i18n.menuDispatch,
      child: const Center(
        child: Text(
          'Expédition des Colis',
          style: TextStyle(fontSize: 30),
        ),
      ),
    );
  }
}
