import 'package:flutter/material.dart';
import 'package:name_of_project/presentation/util/extension/context_extension.dart';
import 'package:name_of_project/presentation/widget/functional/scaffold/appname_scaffold.dart';

class PriceTrackingPage extends StatelessWidget {
  const PriceTrackingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppNameScaffold(
      title: context.i18n.menuPriceTracking,
      child: const Center(
        child: Text(
          'Repérage des prix',
          style: TextStyle(fontSize: 30),
        ),
      ),
    );
  }
}
