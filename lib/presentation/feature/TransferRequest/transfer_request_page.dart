import 'package:flutter/material.dart';
import 'package:name_of_project/presentation/util/extension/context_extension.dart';
import 'package:name_of_project/presentation/widget/functional/scaffold/appname_scaffold.dart';

class TransferPage extends StatelessWidget {
  const TransferPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppNameScaffold(
      title: context.i18n.menuTransferRequest,
      child: const Center(
        child: Text(
          'Demande de transfert de colis',
          style: TextStyle(fontSize: 30),
        ),
      ),
    );
  }
}
