import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:name_of_project/domain/src/notifier/locale/locale_notifier.dart';
import 'package:name_of_project/l10n/generated/app_localizations.dart';
import 'package:name_of_project/presentation/core/router/appname_router.dart';
import 'package:name_of_project/presentation/util/extension/context_extension.dart';
import 'package:name_of_project/presentation/util/theme/appname_ligth_theme.dart';

class MyApp extends ConsumerWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final locale = ref.watch(localeProvider);

    return MaterialApp.router(
      title: 'Flutter Demo',
      theme: SmaLightTheme().themeData(),
      onGenerateTitle: (context) => context.i18n.appTitle,
      locale: locale,
      supportedLocales: AppLocalizations.supportedLocales,
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      routerConfig: smaRouter,
    );
  }
}
