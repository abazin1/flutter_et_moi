import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:name_of_project/presentation/feature/login/view/login_view.dart';
import 'package:name_of_project/presentation/widget/functional/scaffold/appname_background.dart';

class LoginPage extends ConsumerWidget {
  LoginPage({super.key});

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primaryContainer,
      ),
      body: AppNameBackground(
        child: LoginView(
          formKey: _formKey,
          onValidate: () => validate(ref),
        ),
      ),
    );
  }

  bool validate(WidgetRef ref) {
    return true;
    // Used in case of login
    //   if (_formKey.currentState?.validate() ?? false) {
    //     _formKey.currentState?.save();

    //     final model = ref.read(signProvider);
    //     return ref.read(signServiceProvider).validate(model);
    //   }
    //   return false;
    // }
  }
}
