import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:go_router/go_router.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
// import 'package:name_of_project/domain/src/notifier/profile/profile_notifier.dart';
import 'package:name_of_project/presentation/core/router/appname_route.dart';
import 'package:name_of_project/presentation/feature/login/notifier/sign_model_notifier.dart';
import 'package:name_of_project/presentation/util/extension/context_extension.dart';
import 'package:name_of_project/presentation/util/theme/appname_palette.dart';

typedef OnValidate = bool Function();

class LoginView extends HookConsumerWidget {
  const LoginView({
    super.key,
    required this.formKey,
    required this.onValidate,
  });

  final GlobalKey<FormState> formKey;
  final OnValidate onValidate;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final nameController = useTextEditingController();
    final nameFocusNode = useFocusNode();

    final passwordController = useTextEditingController.fromValue(const TextEditingValue(text: '0000'));
    final passwordFocusNode = useFocusNode();

    return Form(
      key: formKey,
      child: Container(
        padding: const EdgeInsets.all(23.0),
        margin: const EdgeInsets.all(23.0),
        decoration: BoxDecoration(
          color: lightColorScheme.primaryContainer,
          borderRadius: const BorderRadius.all(Radius.circular(10)),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Icon(Icons.settings),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text(
                context.i18n.unlockDialogBody,
                style: Theme.of(context).textTheme.headlineLarge,
              ),
            ),
            TextFormField(
              controller: nameController,
              focusNode: nameFocusNode,
              decoration: InputDecoration(
                labelText: context.i18n.unlockLabelNameField,
              ),
              onSaved: (_) {
                ref.read(signProvider.notifier).setLogin = nameController.text;
              },
            ),
            const SizedBox(height: 8),
            TextFormField(
              controller: passwordController,
              focusNode: passwordFocusNode,
              obscureText: true,
              decoration: InputDecoration(
                labelText: context.i18n.unlockLabelPasswordField,
              ),
              onSaved: (_) {
                ref.read(signProvider.notifier).setPassword = passwordController.text;
              },
            ),
            const SizedBox(height: 8),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                ElevatedButton(
                  onPressed: () => context.pushNamed(kWelcomePage),
                  child: const Text('OK'),
                ),
                const SizedBox(width: 8),
                OutlinedButton(
                  onPressed: () => context.pushNamed(kWelcomePage), // Change to help page
                  child: Text(context.i18n.helpButton),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
