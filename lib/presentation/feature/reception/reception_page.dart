import 'package:flutter/material.dart';
import 'package:name_of_project/presentation/util/extension/context_extension.dart';
import 'package:name_of_project/presentation/widget/functional/scaffold/appname_scaffold.dart';
// import 'package:name_of_project/presentation/widget/functional/scaffold/appname_scaffold.dart';

class ReceptionPage extends StatelessWidget {
  const ReceptionPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppNameScaffold(
      title: context.i18n.menuReception,
      child: const Center(
        child: Text(
          'Réception Colis',
          style: TextStyle(fontSize: 30),
        ),
      ),
    );
  }
}
