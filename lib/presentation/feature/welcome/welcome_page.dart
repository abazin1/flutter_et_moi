// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:name_of_project/domain/src/notifier/profile/profile_notifier.dart';
import 'package:name_of_project/presentation/core/router/appname_route.dart';
// import 'package:name_of_project/presentation/notifier/loading_notifier.dart';
import 'package:name_of_project/presentation/util/extension/context_extension.dart';
// import 'package:name_of_project/presentation/widget/functional/card/appname_card.dart';
import 'package:name_of_project/presentation/widget/functional/scaffold/appname_scaffold.dart';

class WelcomePage extends HookConsumerWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final profile = ref.watch(profileNotifierProvider);

    // final loading = ref.watch(loadingProvider);

    IconData placeholderIcon = Icons.image;

    DateTime lastSyncDate = DateTime.now().subtract(const Duration(days: 3));

    return AppNameScaffold(
      title: context.i18n.welcomeTitle(profile.name),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Row(
              children: [
                Expanded(
                  child: menu_card(placeholderIcon: Icons.trolley, menuTitle: context.i18n.menuReception, route: kReception),
                ),
                const SizedBox(width: 20),
                Expanded(
                  child: menu_card(placeholderIcon: placeholderIcon, menuTitle: context.i18n.menuDispatch, route: kDispatch),
                ),
              ],
            ),
            const SizedBox(height: 10),
            Row(
              children: [
                Expanded(
                  child: menu_card(placeholderIcon: placeholderIcon, menuTitle: context.i18n.menuTransferRequest, route: kTransfer),
                ),
                const SizedBox(width: 20),
                Expanded(
                  child: menu_card(placeholderIcon: placeholderIcon, menuTitle: context.i18n.menuPriceTracking, route: kPriceTracking),
                ),
              ],
            ),
            const SizedBox(height: 10),
            Row(
              children: [
                Expanded(
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: menu_card(placeholderIcon: placeholderIcon, menuTitle: context.i18n.menuArticleSheet, route: kArticleSheet),
                  ),
                ),
                const SizedBox(width: 20),
                Expanded(child: menu_card(placeholderIcon: placeholderIcon, menuTitle: context.i18n.menuadd, route: kReception)),
              ],
            ),
            const SizedBox(height: 10),
            ElevatedButton(
              onPressed: () {
                _syncData(ref);
              },
              child: Text(
                'Dernière synchro: ${DateFormat.yMMMd().format(lastSyncDate)}',
                style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Future<void> onTapAction1(WidgetRef ref) async {
  //   try {
  //     print('Welcome');
  //   } finally {
  //     print('end');
  //   }
  // }

  // Future<void> _onTapAction2(WidgetRef ref) async {
  //   try {
  //     print('Welcome');
  //   } finally {
  //     print('end');
  //   }
  // }

  void _syncData(WidgetRef ref) {
    ref.read(profileNotifierProvider.notifier).updateLastSyncDate(DateTime.now());
  }
}

class menu_card extends StatelessWidget {
  const menu_card({
    super.key,
    // ignore: non_constant_identifier_names
    required this.placeholderIcon,
    // ignore: non_constant_identifier_names
    required this.menuTitle,
    required this.route,
  });

  final IconData placeholderIcon;
  final String menuTitle; //Todo: type
  final String route;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(placeholderIcon, size: 50, color: Colors.grey),
            const SizedBox(height: 15),
            Text(menuTitle, textAlign: TextAlign.center),
          ],
        ),
        onTap: () => context.goNamed(route),
      ),
    );
  }
}
