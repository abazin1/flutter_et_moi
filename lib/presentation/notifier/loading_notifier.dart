import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'loading_notifier.g.dart';

@Riverpod(keepAlive: true)
class Loading extends _$Loading {
  @override
  bool build() => false;

  void loading() {
    state = true;
  }

  void done() {
    state = false;
  }
}
