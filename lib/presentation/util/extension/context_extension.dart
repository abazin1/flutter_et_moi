import 'package:flutter/material.dart';
import 'package:name_of_project/l10n/generated/app_localizations.dart';

extension I18nExtension on BuildContext {
  AppLocalizations get i18n => AppLocalizations.of(this);
}
