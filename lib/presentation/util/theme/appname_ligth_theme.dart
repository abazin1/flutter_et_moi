import 'package:flutter/material.dart';
import 'package:name_of_project/presentation/util/theme/appname_palette.dart';

class SmaLightTheme {
  AppBarTheme get appBarThemData => const AppBarTheme();

  BottomAppBarTheme bottomAppBarThemeData = const BottomAppBarTheme();

  CardTheme get cardThemeData => const CardTheme(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
      );

  CheckboxThemeData get checkboxThemeData => const CheckboxThemeData();

  DividerThemeData get dividerThemeData => const DividerThemeData();

  DrawerThemeData get drawerThemeData => const DrawerThemeData();

  ElevatedButtonThemeData get elevatedButtonThemeData => ElevatedButtonThemeData(
        style: ButtonStyle(
          textStyle: MaterialStateProperty.resolveWith((states) {
            return TextStyle(color: lightColorScheme.onSurfaceVariant, fontSize: 16, fontFamily: 'Roboto', fontWeight: FontWeight.w500);
          }),
          backgroundColor: MaterialStateProperty.resolveWith((states) {
            if (states.contains(MaterialState.disabled)) {
              return lightColorScheme.secondary;
            }
            return lightColorScheme.primary;
          }),
          foregroundColor: MaterialStateProperty.resolveWith((states) {
            if (states.contains(MaterialState.disabled)) {
              return lightColorScheme.onSecondary;
            }
            return lightColorScheme.onPrimary;
          }),
        ),
      );

  IconThemeData get iconThemeData => IconThemeData(
        size: 22,
        color: lightColorScheme.onSecondaryContainer,
      );

  InputDecorationTheme get inputDecorationThemeData => InputDecorationTheme(
        filled: true,
        fillColor: Colors.white,
        border: const UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.black),
        ),
        contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
        labelStyle: TextStyle(color: lightColorScheme.onSurface, fontSize: 16, fontFamily: 'Roboto', fontWeight: FontWeight.w500),
      );

  OutlinedButtonThemeData get outlinedButtonThemeData => OutlinedButtonThemeData(
        style: ButtonStyle(
          textStyle: MaterialStateProperty.resolveWith((states) {
            return TextStyle(color: lightColorScheme.onSurfaceVariant, fontSize: 16, fontFamily: 'Roboto', fontWeight: FontWeight.w500);
          }),
          backgroundColor: MaterialStateProperty.resolveWith((states) {
            if (states.contains(MaterialState.disabled)) {
              return lightColorScheme.secondary;
            }
            return lightColorScheme.tertiary;
          }),
          foregroundColor: MaterialStateProperty.resolveWith((states) {
            if (states.contains(MaterialState.disabled)) {
              return lightColorScheme.onSecondary;
            }
            return lightColorScheme.onPrimary;
          }),
        ),
      );

  PopupMenuThemeData get popupMenuThemeData => const PopupMenuThemeData();

  RadioThemeData get radioThemeData => const RadioThemeData();

  ScrollbarThemeData get scrollbarThemeData => const ScrollbarThemeData();

  SliderThemeData get sliderThemeData => const SliderThemeData();

  SwitchThemeData get switchThemeData => const SwitchThemeData();

  TabBarTheme get tabBarThemeData => const TabBarTheme();

  TextButtonThemeData get textButtonThemeData => const TextButtonThemeData();

  TextSelectionThemeData get textSelectionThemeData => const TextSelectionThemeData();

  TextTheme get textThemeData => TextTheme(
        headlineLarge: TextStyle(color: lightColorScheme.onSurface, fontSize: 22, fontFamily: 'Roboto', fontWeight: FontWeight.w400),
        headlineMedium: TextStyle(color: lightColorScheme.onSurfaceVariant, fontSize: 16, fontFamily: 'Roboto', fontWeight: FontWeight.w500),
        headlineSmall: TextStyle(color: lightColorScheme.onSurfaceVariant, fontSize: 10, fontFamily: 'Roboto', fontWeight: FontWeight.w600),
        displayLarge: TextStyle(color: lightColorScheme.onSurface, fontSize: 22, fontFamily: 'Roboto', fontWeight: FontWeight.w400),
        displayMedium: TextStyle(color: lightColorScheme.onSurface, fontSize: 16, fontFamily: 'Roboto', fontWeight: FontWeight.w500),
        displaySmall: TextStyle(color: lightColorScheme.onSurface, fontSize: 10, fontFamily: 'Roboto', fontWeight: FontWeight.w600),
        titleLarge: TextStyle(color: lightColorScheme.onSurface, fontSize: 22, fontFamily: 'Roboto', fontWeight: FontWeight.w400),
        titleMedium: TextStyle(color: lightColorScheme.onSurface, fontSize: 16, fontFamily: 'Roboto', fontWeight: FontWeight.w500),
        titleSmall: TextStyle(color: lightColorScheme.onSurface, fontSize: 10, fontFamily: 'Roboto', fontWeight: FontWeight.w600),
        labelLarge: TextStyle(color: lightColorScheme.onSurface, fontSize: 22, fontFamily: 'Roboto', fontWeight: FontWeight.w400),
        labelMedium: TextStyle(color: lightColorScheme.onSurface, fontSize: 16, fontFamily: 'Roboto', fontWeight: FontWeight.w500),
        labelSmall: TextStyle(color: lightColorScheme.onSurface, fontSize: 10, fontFamily: 'Roboto', fontWeight: FontWeight.w600),
        bodyLarge: TextStyle(color: lightColorScheme.onSurface, fontSize: 22, fontFamily: 'Roboto', fontWeight: FontWeight.w400),
        bodyMedium: TextStyle(color: lightColorScheme.onSurface, fontSize: 16, fontFamily: 'Roboto', fontWeight: FontWeight.w500),
        bodySmall: TextStyle(color: lightColorScheme.onSurface, fontSize: 10, fontFamily: 'Roboto', fontWeight: FontWeight.w600),
      );

  ThemeData themeData() {
    return ThemeData(
      // fontFamily: ,
      brightness: Brightness.light,
      colorScheme: lightColorScheme,
      appBarTheme: appBarThemData,
      bottomAppBarTheme: bottomAppBarThemeData,
      cardTheme: cardThemeData,
      checkboxTheme: checkboxThemeData,
      dividerTheme: dividerThemeData,
      drawerTheme: drawerThemeData,
      elevatedButtonTheme: elevatedButtonThemeData,
      iconTheme: iconThemeData,
      inputDecorationTheme: inputDecorationThemeData,
      outlinedButtonTheme: outlinedButtonThemeData,
      popupMenuTheme: popupMenuThemeData,
      radioTheme: radioThemeData,
      scrollbarTheme: scrollbarThemeData,
      sliderTheme: sliderThemeData,
      switchTheme: switchThemeData,
      tabBarTheme: tabBarThemeData,
      textButtonTheme: textButtonThemeData,
      textSelectionTheme: textSelectionThemeData,
      textTheme: textThemeData,
    );
  }
}
