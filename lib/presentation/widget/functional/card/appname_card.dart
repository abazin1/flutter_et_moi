import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class AppNameCard extends ConsumerWidget {
  const AppNameCard({
    super.key,
    required this.title,
    this.icon,
    this.onTap,
    this.loading = false,
    required Image leading,
  });

  final String title;
  final GestureTapCallback? onTap;
  final bool loading;
  final IconData? icon;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SizedBox(
      height: 400,
      child: Card(
        child: InkWell(
          borderRadius: const BorderRadius.all(Radius.circular(8)),
          onTap: loading ? null : onTap,
          child: Center(
            child: loading
                ? const CircularProgressIndicator()
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      if (icon != null) Icon(icon),
                      Text(
                        title,
                        style: Theme.of(context).textTheme.headlineSmall,
                      ),
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}
